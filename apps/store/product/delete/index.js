export const state = () => ({});
export const mutations = {};
export const actions = {
  async deleteImagePdo({ commit }, data) {
    const res = await this.$axios.$post("api/v1/delete-image-product-other", data);
    return res;
  },
};

export const getters = {};
