export const state = () => ({});
export const mutations = {};
export const actions = {
  async addCarousel({ commit, state }, data) {
    // console.log(data);
    if (data.image) {
      let formData = new FormData();
      formData.append("rating", 0);
      formData.append("carousel_alt", data.carousel_alt);
      data.image.map((dataImage) => {
        formData.append("image", dataImage);
      });
      const res = await this.$axios.$post(
        "homepage/v1/carousel-add",
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );
      return res;
    }
    // else {
    //   const res = await this.$axios.$post("api/create-product", data);
    //   if (res.success) {
    //     return res;
    //   }
    // }
  },
};

export const getters = {};
