export const state = () => ({});
export const mutations = {};

export const actions = {
  async selectBrands({ commit }) {
    const res = await this.$axios.$get("brands/v1/brands");
    return res;
  },
  async selectOne({ commit }, data) {
    const id = { id: data.id };
    // console.log(id);
    const res = await this.$axios.$post("brands/v1/brands/by-id", id);
    return res;
  },
};

export const getters = {};
