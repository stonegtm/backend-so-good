export const state = () => ({});
export const mutations = {};
export const actions = {
    async addPromotion({ commit, state }, data) {
        const formData = new FormData();
        formData.append("name", data.name);
        formData.append("image", data.image);
        formData.append("banner_pc", data.banner_pc);
        formData.append("banner_mobile", data.banner_mobile);
        const res = await this.$axios.$post("api/v1/add-promotion", formData, {
            headers: {
                "Content-Type": "multipart/form-data"
            }
        });
        return res;
    },
    async promotionHaving({ commit, state }) {
        const res = await this.$axios.$get('api/v1/back-show-data-promotion')
        return res
    },
    async promotionSingle({ commit, state }, data) {
        const res = await this.$axios.$get(`api/v1/back-show-data-promotion-single/${data}`)
        return res
    },
    async addProductPromotion({ commit, state }, data) {
        const res = await this.$axios.$post('api/v1/back-add-product-promotion', data)
        return res
    },
    async getProductPromotion({ commit, state }, data) {
        const res = await this.$axios.$post('api/v1/get-promotion-product-picked', data)
        return res
    },
    async editDiscountProductPromotion({ commit, state }, data) {
        const res = await this.$axios.$post('api/v1/edit-discount-product-promotion', data)
        return res
    },
    async deleteProductPromotion({ commit, state }, data) {
        const res = await this.$axios.$post('api/v1/delete-product-promotion', data)
        return res
    }
    ,
    async nameProductForAdd({ commit, state }) {
        const res = await this.$axios.$get("api/v1/name-for-add-product-promotion");
        return res;
    },

    async dataProductOther({ commit }, data) {
        const res = await this.$axios.$post("api/v1/promotion_other_product", data);
        return res;
    },

    async savePromotionProductOther({ commit }, data) {
        const res = await this.$axios.$post("api/v1/add_promotion_other_product", data);
        return res;
    },

    async dataPromotionSingle({ commit }, data) {
        const res = await this.$axios.$post("api/v1/get_promotion_single", data);
        return res;
    },
    

}