export const state = () => ({});
export const mutations = {};
export const actions = {
  async editStatus({ commit, state }, data) {
    if (data.newImg) {
      const formData = new FormData();
      formData.append("id", data.id);
      formData.append("name", data.name);
      formData.append("meta_title", data.meta_title);
      formData.append("meta_description", data.meta_description);
      formData.append("meta_keywords", data.meta_keywords);
      formData.append("img_cover", data.img_cover);
      formData.append("newImg", data.newImg);
      const res = await this.$axios.$post(
        "api/categories-list-edit",
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );
      return res;
    } else {
      const res = await this.$axios.$post("api/categories-list-edit", data);
      return res;
    }
  },
};

export const getters = {};
