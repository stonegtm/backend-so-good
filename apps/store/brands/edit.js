export const state = () => ({});
export const mutations = {};

export const actions = {
  async editBrandStatus({ commit }, data) {
    if (data.newImg) {
      const formData = new FormData();
      formData.append("id", data.id);
      formData.append("name", data.name);
      formData.append("img", data.img);
      formData.append("meta_title", data.meta_title);
      formData.append("meta_description", data.meta_description);
      formData.append("meta_keywords", data.meta_keywords);
      formData.append("newImg", data.newImg);
      const res = await this.$axios.$post(
        "api/v1/brands/edit-status",
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );
      return res;
    } else {
      const res = await this.$axios.$post("api/v1/brands/edit-status", data);
      return res;
    }
  },
};

export const getters = {};
