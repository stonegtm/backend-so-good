export const state = () => ({});
export const mutations = {};

export const actions = {
  async addBrands({ state }, data) {
    if (data.img) {
      const formData = new FormData();
      formData.append("name", data.name);
      formData.append("status", data.status);
      formData.append("img", data.img);
      formData.append("meta_title", data.meta_title);
      formData.append("meta_description", data.meta_description);
      formData.append("meta_keywords", data.meta_keywords);
      const res = await this.$axios.$post("api/create-brand", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });
      return res;
    } else {
      const res = await this.$axios.$post("api/create-brand", data);
      if (res.success) {
        return res;
      } else {
        return res;
      }
    }
  },
};

export const getters = {};
