export { default as VuetifyLogo } from '../../components/VuetifyLogo.vue'
export { default as CustomerTable } from '../../components/customer/customer-table.vue'
export { default as CategoriesFormAdd } from '../../components/categories/form_add.vue'
export { default as CategoriesTable } from '../../components/categories/table.vue'
export { default as DashboardCard } from '../../components/dashboard/card.vue'
export { default as HomepageFeatureItem } from '../../components/homepage/featureItem.vue'
export { default as HomepageTableCarousel } from '../../components/homepage/table-carousel.vue'
export { default as StockTable } from '../../components/stock/table.vue'
export { default as ProductCombobox } from '../../components/product/combobox.vue'
export { default as ProductTableProduct } from '../../components/product/table-product.vue'
export { default as BrandsFormAdd } from '../../components/brands/form_add.vue'
export { default as BrandsTableBrands } from '../../components/brands/table-brands.vue'

// nuxt/nuxt.js#8607
function wrapFunctional(options) {
  if (!options || !options.functional) {
    return options
  }

  const propKeys = Array.isArray(options.props) ? options.props : Object.keys(options.props || {})

  return {
    render(h) {
      const attrs = {}
      const props = {}

      for (const key in this.$attrs) {
        if (propKeys.includes(key)) {
          props[key] = this.$attrs[key]
        } else {
          attrs[key] = this.$attrs[key]
        }
      }

      return h(options, {
        on: this.$listeners,
        attrs,
        props,
        scopedSlots: this.$scopedSlots,
      }, this.$slots.default)
    }
  }
}
