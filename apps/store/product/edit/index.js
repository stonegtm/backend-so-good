
export const state = () => ({});
export const mutations = {};
export const actions = {
    async editProductOther({ commit }, data) {
        const res = await this.$axios.$post("api/v1/product-other-sigle-backend", data);
        return res;
    },
    async editDataProductOther({ commit }, data) {
        if (data.image) {
            let formData = new FormData();
            formData.append("id", data.id);
            formData.append("product_id", data.product_id);
            formData.append("price", data.price);
            formData.append("discount", data.discount);
            formData.append("color", data.color);
            formData.append("code_color", data.code_color);
            formData.append("size", data.size);
            formData.append("code_size", data.size);
            formData.append("quantity", data.quantity);
            //ลูปส่งรูปแบบหลายรูป
            data.image.map((dataImage) => {
                formData.append("image", dataImage);
            });
            const res = await this.$axios.$post("api/v1/edit-product-other-sigle-backend", formData, {
                headers: {
                    "Content-Type": "multipart/form-data",
                },
            });
            return res;

        } else {
            const res = await this.$axios.$post("api/v1/edit-product-other-sigle-backend", data);
            return res;
        }
    },
};

export const getters = {};
