export const state = () => ({});
export const mutations = {};
export const actions = {

  async pickedYouMayAlsoLike({ commit, state }) {
    const res = await this.$axios.$get("api/v1/you-may-also-like");
    return res;
  },
  async addYouMayAlsoLike({ commit },data) {
    const res = await this.$axios.$post("api/v1/add-you-may-also-like",data);
    return res;
  },
  async deletePickedYouMayAlsoLike({ commit }, data) {
    const res = await this.$axios.$post("api/v1/delete-you-may-also-like", data);
    return res;
  },

};

export const getters = {};
