export const state = () => ({});
export const mutations = {};
export const actions = {
  async deleteImage({ commit }, data) {
    const res = await this.$axios.$post("api/v1/product/delete-image", data);
    return res;
  },
  async deleteImageDetail({ commit }, data) {
    const res = await this.$axios.$post("api/v1/product/delete-image-detail", data);
    return res;
  },

};

export const getters = {};
