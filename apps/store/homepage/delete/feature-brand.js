export const state = () => ({});
export const mutations = {};
export const actions = {
  async deleteAndEditFeatureBrand({ commit, state }, data) {
    const res = await this.$axios.$post(
      "api/v1/edit-and-delete-feature-brand",
      data
    );
    return res;
  },
};

export const getters = {};
