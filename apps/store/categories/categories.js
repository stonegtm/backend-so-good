export const state = () => ({
  categories: null,
});
export const mutations = {
  CATEGORIES(state, payload) {
    state.categories = payload;
  },
};

export const actions = {
  async addCategories({ state }, data) {
    if (data.img_cover) {
      //   console.log(data);
      const formData = new FormData();
      formData.append("name", data.name);
      formData.append("displayed", data.displayed);
      formData.append("main", data.main);
      formData.append("meta_title", data.meta_title);
      formData.append("meta_description", data.meta_description);
      formData.append("meta_keywords", data.meta_keywords);
      formData.append("img_cover", data.img_cover);
      const res = await this.$axios.$post("api/create-categories", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });
      return res;
    } else {
      const res = await this.$axios.$post("api/create-categories", data);
      if (res.success) {
        return res;
      } else {
        return res;
      }
    }
  },
  //SELECT CATEGORY ONLY NAME START
  async getCategories({ commit }) {
    const res = await this.$axios.$get("api/categories");
    commit("CATEGORIES", res);
  },
  //SELECT CATEGORY ONLY NAME END
};

export const getters = {
  //SELECT CATEGORY ONLY NAME START
  catagoryMain(state) {
    return state.categories;
  },
  //SELECT CATEGORY ONLY NAME END
};
