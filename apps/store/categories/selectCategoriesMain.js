export const state = () => ({
  // categoriesMain: null,
});
export const mutations = {
  // SELECT_CATEGORIES_MAIN(state, payload) {
  //   state.categoriesMain = payload;
  //   console.log("playload",payload);
  // },
};
export const actions = {
  async selectCategoriesMain({ commit, state }) {
    const res = await this.$axios.$get("api/categories-main");
    return res;
  },

  async selectOne({ commit }, data) {
    const id = {id:data.id}
    // console.log(id);
    const res = await this.$axios.$post("api/categories-main-by-id", id);
    return res
  },
};

export const getters = {};
