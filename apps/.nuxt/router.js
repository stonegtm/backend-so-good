import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _0028b9e8 = () => interopDefault(import('../pages/brands/index.vue' /* webpackChunkName: "pages/brands/index" */))
const _6dbaf648 = () => interopDefault(import('../pages/categories/index.vue' /* webpackChunkName: "pages/categories/index" */))
const _0485595e = () => interopDefault(import('../pages/customer/index.vue' /* webpackChunkName: "pages/customer/index" */))
const _046aaffc = () => interopDefault(import('../pages/login.vue' /* webpackChunkName: "pages/login" */))
const _5ff8091c = () => interopDefault(import('../pages/payment/index.vue' /* webpackChunkName: "pages/payment/index" */))
const _7ad7d4c5 = () => interopDefault(import('../pages/product/index.vue' /* webpackChunkName: "pages/product/index" */))
const _363400b9 = () => interopDefault(import('../pages/promotion/index.vue' /* webpackChunkName: "pages/promotion/index" */))
const _154b9944 = () => interopDefault(import('../pages/review-content/index.vue' /* webpackChunkName: "pages/review-content/index" */))
const _c5d1c27a = () => interopDefault(import('../pages/seoulgoodspick/index.vue' /* webpackChunkName: "pages/seoulgoodspick/index" */))
const _7b6f5cec = () => interopDefault(import('../pages/stock/index.vue' /* webpackChunkName: "pages/stock/index" */))
const _4c6b2d2b = () => interopDefault(import('../pages/you_may_also_like/index.vue' /* webpackChunkName: "pages/you_may_also_like/index" */))
const _17cec8a4 = () => interopDefault(import('../pages/brands/add-brands.vue' /* webpackChunkName: "pages/brands/add-brands" */))
const _157a9390 = () => interopDefault(import('../pages/categories/add-category.vue' /* webpackChunkName: "pages/categories/add-category" */))
const _2b964902 = () => interopDefault(import('../pages/homepage/best-sellers.vue' /* webpackChunkName: "pages/homepage/best-sellers" */))
const _38b6bb18 = () => interopDefault(import('../pages/homepage/carousel.vue' /* webpackChunkName: "pages/homepage/carousel" */))
const _7fa5e2bf = () => interopDefault(import('../pages/homepage/featured-brand/index.vue' /* webpackChunkName: "pages/homepage/featured-brand/index" */))
const _cb40c530 = () => interopDefault(import('../pages/homepage/new-arrival.vue' /* webpackChunkName: "pages/homepage/new-arrival" */))
const _11c952f6 = () => interopDefault(import('../pages/product/add-product.vue' /* webpackChunkName: "pages/product/add-product" */))
const _c38968a4 = () => interopDefault(import('../pages/review-content/add-review-content.vue' /* webpackChunkName: "pages/review-content/add-review-content" */))
const _63ade1dd = () => interopDefault(import('../pages/stock/add-product.vue' /* webpackChunkName: "pages/stock/add-product" */))
const _657c8237 = () => interopDefault(import('../pages/homepage/featured-brand/add-featured-brand.vue' /* webpackChunkName: "pages/homepage/featured-brand/add-featured-brand" */))
const _44f128f1 = () => interopDefault(import('../pages/product/other/edit/_id.vue' /* webpackChunkName: "pages/product/other/edit/_id" */))
const _283a8c2b = () => interopDefault(import('../pages/customer/address/_id.vue' /* webpackChunkName: "pages/customer/address/_id" */))
const _6498daff = () => interopDefault(import('../pages/customer/in-cart/_id.vue' /* webpackChunkName: "pages/customer/in-cart/_id" */))
const _b9d1d1b8 = () => interopDefault(import('../pages/product/add-product-other/_id.vue' /* webpackChunkName: "pages/product/add-product-other/_id" */))
const _51e32412 = () => interopDefault(import('../pages/product/edit/_id.vue' /* webpackChunkName: "pages/product/edit/_id" */))
const _1f9a702e = () => interopDefault(import('../pages/product/other/_id.vue' /* webpackChunkName: "pages/product/other/_id" */))
const _5fc4125c = () => interopDefault(import('../pages/promotion/other_product/_id.vue' /* webpackChunkName: "pages/promotion/other_product/_id" */))
const _17e29b33 = () => interopDefault(import('../pages/review-content/edit/_id.vue' /* webpackChunkName: "pages/review-content/edit/_id" */))
const _d853de3a = () => interopDefault(import('../pages/categories/_categorieslist.vue' /* webpackChunkName: "pages/categories/_categorieslist" */))
const _4d20015e = () => interopDefault(import('../pages/promotion/_name.vue' /* webpackChunkName: "pages/promotion/_name" */))
const _3ac30ce5 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/brands",
    component: _0028b9e8,
    name: "brands"
  }, {
    path: "/categories",
    component: _6dbaf648,
    name: "categories"
  }, {
    path: "/customer",
    component: _0485595e,
    name: "customer"
  }, {
    path: "/login",
    component: _046aaffc,
    name: "login"
  }, {
    path: "/payment",
    component: _5ff8091c,
    name: "payment"
  }, {
    path: "/product",
    component: _7ad7d4c5,
    name: "product"
  }, {
    path: "/promotion",
    component: _363400b9,
    name: "promotion"
  }, {
    path: "/review-content",
    component: _154b9944,
    name: "review-content"
  }, {
    path: "/seoulgoodspick",
    component: _c5d1c27a,
    name: "seoulgoodspick"
  }, {
    path: "/stock",
    component: _7b6f5cec,
    name: "stock"
  }, {
    path: "/you_may_also_like",
    component: _4c6b2d2b,
    name: "you_may_also_like"
  }, {
    path: "/brands/add-brands",
    component: _17cec8a4,
    name: "brands-add-brands"
  }, {
    path: "/categories/add-category",
    component: _157a9390,
    name: "categories-add-category"
  }, {
    path: "/homepage/best-sellers",
    component: _2b964902,
    name: "homepage-best-sellers"
  }, {
    path: "/homepage/carousel",
    component: _38b6bb18,
    name: "homepage-carousel"
  }, {
    path: "/homepage/featured-brand",
    component: _7fa5e2bf,
    name: "homepage-featured-brand"
  }, {
    path: "/homepage/new-arrival",
    component: _cb40c530,
    name: "homepage-new-arrival"
  }, {
    path: "/product/add-product",
    component: _11c952f6,
    name: "product-add-product"
  }, {
    path: "/review-content/add-review-content",
    component: _c38968a4,
    name: "review-content-add-review-content"
  }, {
    path: "/stock/add-product",
    component: _63ade1dd,
    name: "stock-add-product"
  }, {
    path: "/homepage/featured-brand/add-featured-brand",
    component: _657c8237,
    name: "homepage-featured-brand-add-featured-brand"
  }, {
    path: "/product/other/edit/:id?",
    component: _44f128f1,
    name: "product-other-edit-id"
  }, {
    path: "/customer/address/:id?",
    component: _283a8c2b,
    name: "customer-address-id"
  }, {
    path: "/customer/in-cart/:id?",
    component: _6498daff,
    name: "customer-in-cart-id"
  }, {
    path: "/product/add-product-other/:id?",
    component: _b9d1d1b8,
    name: "product-add-product-other-id"
  }, {
    path: "/product/edit/:id?",
    component: _51e32412,
    name: "product-edit-id"
  }, {
    path: "/product/other/:id?",
    component: _1f9a702e,
    name: "product-other-id"
  }, {
    path: "/promotion/other_product/:id?",
    component: _5fc4125c,
    name: "promotion-other_product-id"
  }, {
    path: "/review-content/edit/:id",
    component: _17e29b33,
    name: "review-content-edit-id"
  }, {
    path: "/categories/:categorieslist",
    component: _d853de3a,
    name: "categories-categorieslist"
  }, {
    path: "/promotion/:name",
    component: _4d20015e,
    name: "promotion-name"
  }, {
    path: "/",
    component: _3ac30ce5,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
