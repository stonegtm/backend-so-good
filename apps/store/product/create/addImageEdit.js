export const state = () => ({});
export const mutations = {};
export const actions = {
  async addImageEdit({ commit, state }, data) {
    // console.log(data.image.image);
    let formData = new FormData();
    data.image.image.map((dataImage) => {
      formData.append("image", dataImage);
      formData.append("main", data.main);
    });
    formData.append("product_id", data.product_id);
    const res = await this.$axios.$post(
      "api/v1/product/add-image-other",
      formData,
      {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      }
    );
    return res;
  },
  async addImageDetailEdit({ commit, state }, data) {
    let formData = new FormData();
    data.image.image.map((dataImage) => {
      formData.append("image", dataImage);
    });
    formData.append("product_id", data.product_id);
    const res = await this.$axios.$post(
      "api/v1/product/add-image-detail-other",
      formData,
      {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      }
    );
    return res;
  },
};

export const getters = {};
