export const state = () => ({});
export const mutations = {};
export const actions = {
  async logout({ commit }) {
    const res = await this.$axios.$post("api/v1/logout-employee");
    return res;
  },
};

export const getters = {};
