export const state = () => ({});
export const mutations = {};
export const actions = {
  async getPaymentBackend({ commit }) {
    const res = await this.$axios.$get("api/v1/payment-backend");
    return res;
  },
  async getProductAndAdress({ commit }, data) {
    const res = await this.$axios.$post("api/v1/get-product-and-address", data);
    return res;
  },
  async editStatus({ commit }, data) {
    const res = await this.$axios.$post("api/v1/edit-status-payment", data);
    return res;
  },
  async addNotePayment({ commit }, data) {
    const res = await this.$axios.$post("api/v1/add-note-product", data);
    return res;
  },
  async cutProductStock({ commit }, data) {
    const res = await this.$axios.$post("api/v1/cut-product-in-stock", data);
    return res;
  }
};

export const getters = {};
