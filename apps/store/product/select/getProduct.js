export const state = () => ({});
export const mutations = {};
export const actions = {
  async selectProduct({ commit, state }) {
    const res = await this.$axios.$get("api/v1/product/get-all");
    return res;
  },
  async showImage({ commit, state }, data) {
    const res = await this.$axios.$post("api/v1/product/show-image", data);
    return res;
  },

  async getProductForEdit({ commit, state }, data) {
    const res = await this.$axios.$get(`api/v1/product/get-product/${data}`);
    return res;
  },
};

export const getters = {};
