# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<VuetifyLogo>` | `<vuetify-logo>` (components/VuetifyLogo.vue)
- `<CustomerTable>` | `<customer-table>` (components/customer/customer-table.vue)
- `<CategoriesFormAdd>` | `<categories-form-add>` (components/categories/form_add.vue)
- `<CategoriesTable>` | `<categories-table>` (components/categories/table.vue)
- `<DashboardCard>` | `<dashboard-card>` (components/dashboard/card.vue)
- `<HomepageFeatureItem>` | `<homepage-feature-item>` (components/homepage/featureItem.vue)
- `<HomepageTableCarousel>` | `<homepage-table-carousel>` (components/homepage/table-carousel.vue)
- `<StockTable>` | `<stock-table>` (components/stock/table.vue)
- `<ProductCombobox>` | `<product-combobox>` (components/product/combobox.vue)
- `<ProductTableProduct>` | `<product-table-product>` (components/product/table-product.vue)
- `<BrandsFormAdd>` | `<brands-form-add>` (components/brands/form_add.vue)
- `<BrandsTableBrands>` | `<brands-table-brands>` (components/brands/table-brands.vue)
