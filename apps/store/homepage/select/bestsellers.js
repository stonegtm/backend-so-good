export const state = () => ({});
export const mutations = {};
export const actions = {
  async bestSellersGet({ commit, state }) {
    const res = await this.$axios.$get("homepage/v1/backend-best-sellers");
    return res;
  },
  async addShowBestseller({ commit, state }, data) {
    const res = await this.$axios.$post("homepage/v1/backend-add-best-sellers", data);
    return res;
  },
  async bestSellerPicked({ commit, state }) {
    const res = await this.$axios.$get("homepage/v1/backend-picked-best-sellers");
    return res;
  },
  async deletePickedBestsellers({ commit, state }, data) {
    const res = await this.$axios.$post("homepage/v1/backend-delete-best-sellers", data);
    return res;
  },
  async nameProductForAdd({ commit, state }) {
    const res = await this.$axios.$get("homepage/v1/name-product-for-add");
    return res;
  },
  



};

export const getters = {};
