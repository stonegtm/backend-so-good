export const state = () => ({});
export const mutations = {};
export const actions = {
  async editMainImage({ commit }, data) {
      console.log(data);
    const res = await this.$axios.$post("api/v1/product/edit-main-image", data);
    return res;
  },
};

export const getters = {};
