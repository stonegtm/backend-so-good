export const state = () => ({});
export const mutations = {};
export const actions = {
    async addSeoulgoodsPick({ commit, state }, data) {
        const res = await this.$axios.$post("api/v1/add-seoulgoodspick", data);
        return res;
    },
    async pickedSeoulgoodspick({ commit, state }) {
        const res = await this.$axios.$get("api/v1/back-seoulgoodspick");
        return res;
    },
    async deletePickedSeoulgoodspick({ commit }, data) {
        const res = await this.$axios.$post("api/v1/delete-seoulgoodspick", data);
        return res;
      },
}