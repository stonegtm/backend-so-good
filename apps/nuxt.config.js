import colors from "vuetify/es5/util/colors";

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: "%s - back-app-sogood",
    title: "back-app-sogood",
    htmlAttrs: {
      lang: "en",
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ["@/assets/css/main.css"],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    "@nuxtjs/vuetify",
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: ["@nuxtjs/axios", "@nuxtjs/auth"],
  axios: {
    proxy: true,
  },
  router: {
    middleware: ["auth"],
  },
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: {
            url: "api/v1/login-employee",
            method: "post",
            propertyName: "data.token",
          },
          user: {
            url: "api/v1/employe-data",
            method: "get",
            propertyName: "data",
          },
          logout: false,
        },
      },
    },
    redirect: {
      login: "/login",
    },
  },
  proxy: {
    "api/v1/product": process.env.API_URL || "http://127.0.0.1:3333",
    "/api": process.env.API_URL || "http://127.0.0.1:3333",
    //BRANDS
    "/brands/v1": process.env.API_URL || "http://127.0.0.1:3333",
    "/product/edit/v1": process.env.API_URL || "http://127.0.0.1:3333",
    "/api/v1/brands": process.env.API_URL || "http://127.0.0.1:3333",
    "/homepage/v1": process.env.API_URL || "http://127.0.0.1:3333",
    //TEST DEV
    // "api/v1/product": "http://127.0.0.1:3333",
    // "/api": "http://127.0.0.1:3333",
    // //BRANDS
    // "/brands/v1": "http://127.0.0.1:3333",
    // "/product/edit/v1": "http://127.0.0.1:3333",
    // "/api/v1/brands": "http://127.0.0.1:3333",
    // "/homepage/v1": "http://127.0.0.1:3333",
  },
  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ["~/assets/variables.scss"],
    theme: {
      light: true,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,
        },
      },
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
};
