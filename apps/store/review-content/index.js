export const state = () => ({});
export const mutations = {};
export const actions = {
    async createReview({ commit }, data) {
        if (data.image) {
            const formData = new FormData();
            formData.append("title", data.title);
            formData.append("description_short", data.description_short);
            formData.append("description_full", data.description_full);
            formData.append("image", data.image);
            const res = await this.$axios.$post("api/v1/create-review", formData, {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            });
            if (res.success) {
                return res
            }
        }
    },
    async editReview({ commit }, data) {
        if (data.image) {
            const formData = new FormData();
            formData.append("id", data.id);
            formData.append("title", data.title);
            formData.append("description_short", data.description_short);
            formData.append("description_full", data.description_full);
            formData.append("image", data.image);
            const res = await this.$axios.$post("api/v1/edit-review", formData, {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            });
            if (res.success) {
                return res
            }
        } else {
            const res = await this.$axios.$post("api/v1/edit-review", data);
            if (res.success) {
                return res
            }
        }
    },
    async sigleDataReview({ commit }, data) {
        const res = await this.$axios.$get(`api/v1/get-sigle-review-backend/${data.id}`);
        return res
    },
    async getReview({ commit }) {
        const res = await this.$axios.$get("api/v1/get-review-backend");
        return res
    },
    async deleteReview({ commit }, data) {
        const res = await this.$axios.$post("api/v1/delete-review-backend", data);
        return res
    }
};

export const getters = {};
