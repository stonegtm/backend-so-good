
export const state = () => ({});
export const mutations = {};
export const actions = {
  async editStatusProduct({ commit }, data) {
    const res = await this.$axios.$post("api/v1/product/edit-status-product", data);
    return res;
  },
};

export const getters = {};
