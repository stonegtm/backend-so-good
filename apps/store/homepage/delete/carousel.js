export const state = () => ({});
export const mutations = {};
export const actions = {
  async deleteCarousel({ commit, state }, data) {
    const res = await this.$axios.$post('homepage/v1/carousel-delete',data)
    return res
  },
};

export const getters = {};
