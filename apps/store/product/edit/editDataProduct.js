export const state = () => ({});
export const mutations = {};
export const actions = {
  async editDataProduct({ commit }, data) {
    //   console.log(data);
    const res = await this.$axios.$post("api/v1/product/edit-data-product", data);
    return res;
  },
};

export const getters = {};
