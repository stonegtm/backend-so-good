FROM node:16.13.1-alpine

RUN apk update && apk add bash
# create vartiable directory
ENV ROOT /src 

RUN mkdir ${ROOT}

WORKDIR ${ROOT}

ADD ./apps ${ROOT}

RUN npm install

ENV HOST 0.0.0.0
