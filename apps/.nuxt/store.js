import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const VUEX_PROPERTIES = ['state', 'getters', 'actions', 'mutations']

let store = {};

(function updateModules () {
  // If store is an exported method = classic mode (deprecated)

  if (typeof store === 'function') {
    return console.warn('Classic mode for store/ is deprecated and will be removed in Nuxt 3.')
  }

  // Enforce store modules
  store.modules = store.modules || {}

  resolveStoreModules(require('../store/you_may_also_like/index.js'), 'you_may_also_like/index.js')
  resolveStoreModules(require('../store/seoulgoodspick/index.js'), 'seoulgoodspick/index.js')
  resolveStoreModules(require('../store/review-content/index.js'), 'review-content/index.js')
  resolveStoreModules(require('../store/promotion/index.js'), 'promotion/index.js')
  resolveStoreModules(require('../store/payment/index.js'), 'payment/index.js')
  resolveStoreModules(require('../store/customer/index.js'), 'customer/index.js')
  resolveStoreModules(require('../store/brands/addBrands.js'), 'brands/addBrands.js')
  resolveStoreModules(require('../store/brands/delete.js'), 'brands/delete.js')
  resolveStoreModules(require('../store/brands/edit.js'), 'brands/edit.js')
  resolveStoreModules(require('../store/brands/selectBrands.js'), 'brands/selectBrands.js')
  resolveStoreModules(require('../store/categories/addListCategories.js'), 'categories/addListCategories.js')
  resolveStoreModules(require('../store/categories/categories.js'), 'categories/categories.js')
  resolveStoreModules(require('../store/categories/selectCategoriesMain.js'), 'categories/selectCategoriesMain.js')
  resolveStoreModules(require('../store/employee/auth.js'), 'employee/auth.js')
  resolveStoreModules(require('../store/stock/edit.js'), 'stock/edit.js')
  resolveStoreModules(require('../store/product/edit/index.js'), 'product/edit/index.js')
  resolveStoreModules(require('../store/product/delete/index.js'), 'product/delete/index.js')
  resolveStoreModules(require('../store/categories/delete/listDelete.js'), 'categories/delete/listDelete.js')
  resolveStoreModules(require('../store/categories/delete/mainDelete.js'), 'categories/delete/mainDelete.js')
  resolveStoreModules(require('../store/categories/edit/list.js'), 'categories/edit/list.js')
  resolveStoreModules(require('../store/categories/edit/main.js'), 'categories/edit/main.js')
  resolveStoreModules(require('../store/categories/select/list.js'), 'categories/select/list.js')
  resolveStoreModules(require('../store/homepage/create/carousel.js'), 'homepage/create/carousel.js')
  resolveStoreModules(require('../store/homepage/create/feature-brand.js'), 'homepage/create/feature-brand.js')
  resolveStoreModules(require('../store/homepage/delete/carousel.js'), 'homepage/delete/carousel.js')
  resolveStoreModules(require('../store/homepage/delete/feature-brand.js'), 'homepage/delete/feature-brand.js')
  resolveStoreModules(require('../store/homepage/edit/carousel.js'), 'homepage/edit/carousel.js')
  resolveStoreModules(require('../store/homepage/edit/newarrival.js'), 'homepage/edit/newarrival.js')
  resolveStoreModules(require('../store/homepage/select/bestsellers.js'), 'homepage/select/bestsellers.js')
  resolveStoreModules(require('../store/homepage/select/carousel.js'), 'homepage/select/carousel.js')
  resolveStoreModules(require('../store/homepage/select/newarrival.js'), 'homepage/select/newarrival.js')
  resolveStoreModules(require('../store/product/create/addImageEdit.js'), 'product/create/addImageEdit.js')
  resolveStoreModules(require('../store/product/create/create.js'), 'product/create/create.js')
  resolveStoreModules(require('../store/product/delete/deleteImage.js'), 'product/delete/deleteImage.js')
  resolveStoreModules(require('../store/product/delete/deleteProduct.js'), 'product/delete/deleteProduct.js')
  resolveStoreModules(require('../store/product/edit/editDataProduct.js'), 'product/edit/editDataProduct.js')
  resolveStoreModules(require('../store/product/edit/editMainImage.js'), 'product/edit/editMainImage.js')
  resolveStoreModules(require('../store/product/edit/editStatusProduct.js'), 'product/edit/editStatusProduct.js')
  resolveStoreModules(require('../store/product/select/allBrands.js'), 'product/select/allBrands.js')
  resolveStoreModules(require('../store/product/select/getProduct.js'), 'product/select/getProduct.js')

  // If the environment supports hot reloading...

  if (process.client && module.hot) {
    // Whenever any Vuex module is updated...
    module.hot.accept([
      '../store/you_may_also_like/index.js',
      '../store/seoulgoodspick/index.js',
      '../store/review-content/index.js',
      '../store/promotion/index.js',
      '../store/payment/index.js',
      '../store/customer/index.js',
      '../store/brands/addBrands.js',
      '../store/brands/delete.js',
      '../store/brands/edit.js',
      '../store/brands/selectBrands.js',
      '../store/categories/addListCategories.js',
      '../store/categories/categories.js',
      '../store/categories/selectCategoriesMain.js',
      '../store/employee/auth.js',
      '../store/stock/edit.js',
      '../store/product/edit/index.js',
      '../store/product/delete/index.js',
      '../store/categories/delete/listDelete.js',
      '../store/categories/delete/mainDelete.js',
      '../store/categories/edit/list.js',
      '../store/categories/edit/main.js',
      '../store/categories/select/list.js',
      '../store/homepage/create/carousel.js',
      '../store/homepage/create/feature-brand.js',
      '../store/homepage/delete/carousel.js',
      '../store/homepage/delete/feature-brand.js',
      '../store/homepage/edit/carousel.js',
      '../store/homepage/edit/newarrival.js',
      '../store/homepage/select/bestsellers.js',
      '../store/homepage/select/carousel.js',
      '../store/homepage/select/newarrival.js',
      '../store/product/create/addImageEdit.js',
      '../store/product/create/create.js',
      '../store/product/delete/deleteImage.js',
      '../store/product/delete/deleteProduct.js',
      '../store/product/edit/editDataProduct.js',
      '../store/product/edit/editMainImage.js',
      '../store/product/edit/editStatusProduct.js',
      '../store/product/select/allBrands.js',
      '../store/product/select/getProduct.js',
    ], () => {
      // Update `root.modules` with the latest definitions.
      updateModules()
      // Trigger a hot update in the store.
      window.$nuxt.$store.hotUpdate(store)
    })
  }
})()

// createStore
export const createStore = store instanceof Function ? store : () => {
  return new Vuex.Store(Object.assign({
    strict: (process.env.NODE_ENV !== 'production')
  }, store))
}

function normalizeRoot (moduleData, filePath) {
  moduleData = moduleData.default || moduleData

  if (moduleData.commit) {
    throw new Error(`[nuxt] ${filePath} should export a method that returns a Vuex instance.`)
  }

  if (typeof moduleData !== 'function') {
    // Avoid TypeError: setting a property that has only a getter when overwriting top level keys
    moduleData = Object.assign({}, moduleData)
  }
  return normalizeModule(moduleData, filePath)
}

function normalizeModule (moduleData, filePath) {
  if (moduleData.state && typeof moduleData.state !== 'function') {
    console.warn(`'state' should be a method that returns an object in ${filePath}`)

    const state = Object.assign({}, moduleData.state)
    // Avoid TypeError: setting a property that has only a getter when overwriting top level keys
    moduleData = Object.assign({}, moduleData, { state: () => state })
  }
  return moduleData
}

function resolveStoreModules (moduleData, filename) {
  moduleData = moduleData.default || moduleData
  // Remove store src + extension (./foo/index.js -> foo/index)
  const namespace = filename.replace(/\.(js|mjs)$/, '')
  const namespaces = namespace.split('/')
  let moduleName = namespaces[namespaces.length - 1]
  const filePath = `store/${filename}`

  moduleData = moduleName === 'state'
    ? normalizeState(moduleData, filePath)
    : normalizeModule(moduleData, filePath)

  // If src is a known Vuex property
  if (VUEX_PROPERTIES.includes(moduleName)) {
    const property = moduleName
    const propertyStoreModule = getStoreModule(store, namespaces, { isProperty: true })

    // Replace state since it's a function
    mergeProperty(propertyStoreModule, moduleData, property)
    return
  }

  // If file is foo/index.js, it should be saved as foo
  const isIndexModule = (moduleName === 'index')
  if (isIndexModule) {
    namespaces.pop()
    moduleName = namespaces[namespaces.length - 1]
  }

  const storeModule = getStoreModule(store, namespaces)

  for (const property of VUEX_PROPERTIES) {
    mergeProperty(storeModule, moduleData[property], property)
  }

  if (moduleData.namespaced === false) {
    delete storeModule.namespaced
  }
}

function normalizeState (moduleData, filePath) {
  if (typeof moduleData !== 'function') {
    console.warn(`${filePath} should export a method that returns an object`)
    const state = Object.assign({}, moduleData)
    return () => state
  }
  return normalizeModule(moduleData, filePath)
}

function getStoreModule (storeModule, namespaces, { isProperty = false } = {}) {
  // If ./mutations.js
  if (!namespaces.length || (isProperty && namespaces.length === 1)) {
    return storeModule
  }

  const namespace = namespaces.shift()

  storeModule.modules[namespace] = storeModule.modules[namespace] || {}
  storeModule.modules[namespace].namespaced = true
  storeModule.modules[namespace].modules = storeModule.modules[namespace].modules || {}

  return getStoreModule(storeModule.modules[namespace], namespaces, { isProperty })
}

function mergeProperty (storeModule, moduleData, property) {
  if (!moduleData) {
    return
  }

  if (property === 'state') {
    storeModule.state = moduleData || storeModule.state
  } else {
    storeModule[property] = Object.assign({}, storeModule[property], moduleData)
  }
}
