export const state = () => ({});
export const mutations = {};
export const actions = {
  async customerData({ commit }) {
    const res = await this.$axios.$get("api/v1/customer-backend");
    return res;
  },

  async inCartById({ commit }, data) {
    const res = await this.$axios.$get(`api/v1/customer-incart/${data}`);
    return res;
  },
  async addressCustomer({ commit }, data) {
    const res = await this.$axios.$get(`api/v1/customer-address/${data}`);
    return res;
  },
  async profileCustomer({ commit }, data) {
    const res = await this.$axios.$post(`api/v1/customer-profile/`, data);
    return res;
  },
};

export const getters = {};
