export const state = () => ({});
export const mutations = {};
export const actions = {
  async edtiNewArrival({ commit, state }, data) {
    const res = await this.$axios.$post('homepage/v1/edit-newarrival',data)
    return res
  },
  async editRating({ commit, state }, data) {
    const res = await this.$axios.$post('homepage/v1/edit-rating',data)
    return res
  }
};

export const getters = {};
