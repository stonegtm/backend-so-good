export const state = () => ({});
export const mutations = {};
export const actions = {
  async addProduct({ commit, state }, data) {
    // console.log(data);
    // console.log(data);
    if (data.image) {
      let formData = new FormData();
      formData.append("name", data.name);
      formData.append("color", data.color);
      formData.append("size", data.size);
      formData.append("status", data.status);
      formData.append("brand_id", data.brand_id);
      formData.append("is_delete", data.is_delete);
      formData.append("description", data.description);
      formData.append("price", data.price);
      formData.append("main", data.main);
      formData.append("newarrival_status", data.newarrival_status);
      formData.append("quantity", data.quantity);
      formData.append("discount", data.discount);
      formData.append("meta_title", data.meta_title);
      formData.append("meta_description", data.meta_description);
      formData.append("meta_keywords", data.meta_keywords);
      //ลูปส่งรูปแบบหลายรูป

      formData.append("category", data.category);
      data.image.map((dataImage) => {
        formData.append("image", dataImage);
      });
      data.image_description.map((dataImageDescription) => {
        formData.append("image_description", dataImageDescription);
      });
      const res = await this.$axios.$post("api/create-product", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });
      return res;
    } else {
      const res = await this.$axios.$post("api/create-product", data);
      if (res.success) {
        return res;
      }
    }
  },
  async addProductOtherByStore({ commit, state }, data) {
    if (data.image) {
      let formData = new FormData();
      formData.append("product_id", data.product_id);
      formData.append("price", data.price);
      formData.append("discount", data.discount);
      formData.append("color", data.color);
      formData.append("code_color", data.code_color);
      formData.append("size", data.size);
      formData.append("quantity", data.quantity);
      //ลูปส่งรูปแบบหลายรูป
      data.image.map((dataImage) => {
        formData.append("image", dataImage);
      });
      const res = await this.$axios.$post("api/v1/add-product-other", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });
      return res;
    } else {
      const res = await this.$axios.$post('api/v1/add-product-other', data)
      return res
    }

  }
};

export const getters = {};
