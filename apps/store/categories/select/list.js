export const state = () => ({
  // categoriesMain: null,
});
export const mutations = {
  // SELECT_CATEGORIES_MAIN(state, payload) {
  //   state.categoriesMain = payload;
  //   console.log("playload",payload);
  // },
};
export const actions = {
  async selectCategoriesList({ commit, state }, data) {
    const res = await this.$axios.$post("api/categories-list", data);
    return res;
  },

  async selectOne({ commit }, data) {
    const id = { id: data.id };
    const res = await this.$axios.$post("api/categories-list-by-id", id);
    return res;
  },
};

export const getters = {};
