export const state = () => ({});
export const mutations = {};
export const actions = {
  async addFeatureBrand({ commit }, data) {
    let formData = new FormData();
    formData.append("rating", 0);
    formData.append("image1", data.image1);
    formData.append("image2", data.image2);
    formData.append("alt", data.alt);
    const res = await this.$axios.$post("api/v1/add-feature-brand", formData, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
    return res;
  },
  async getFeatureBrand({ commit }) {
    const res = await this.$axios.$get("api/v1/feature-brand7789c5");
    return res;
  },
};

export const getters = {};
