export const state = () => ({});
export const mutations = {};
export const actions = {
  async deleteProduct({ commit }, data) {
    //   console.log(data);
    const res = await this.$axios.$post("api/v1/product/delete-product", data);
    return res;
  },
};

export const getters = {};
